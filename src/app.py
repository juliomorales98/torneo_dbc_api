from flask import Flask, send_file, request, make_response, jsonify
import sqlite3
import json
import io
from icecream import ic
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


def dbConnect():
    return sqlite3.connect('../../database/torneo_dbc.db')


@app.route("/")
def index():
    return ""


@app.route("/leaderboard", methods=['POST'])
def leaderboard():
    con = dbConnect()
    cursor = con.cursor()
    query = '''
        select a.id,a.name, 
        (select count(*) from game where (player_1 = a.id or player_2 = a.id or player_3 = a.id or player_4 = a.id) 
        and winner > 0 AND tournement = ? ) as jugados, 
        (select count(*) from game where tournement = ? AND ((game_type = 1 AND winner = a.id) OR 
        (game_type = 2 and (player_1 = a.id OR player_2 = a.id) and winner = 1) OR
        ( game_type = 2 and (player_3 = a.id OR player_4 = a.id) and winner = 2) )) as ganados 
        from player a ORDER BY ganados DESC   
    '''
    result = con.execute(query, (request.form.get(
        'tournementId'), request.form.get('tournementId')))
    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
            'played': row[2],
            'wins': row[3],
            'loses': row[2]-row[3]
        })
        json_result = json.dumps(tmp_dic, indent=2)
    return json_result


@app.route("/pending", methods=['POST'])
def pending():
    con = dbConnect()
    cursor = con.cursor()
    query = '''
        select a.id,a.name, 
        (select count(*) from game where (player_1 = a.id or player_2 = a.id or player_3 = a.id or player_4 = a.id) 
        and winner > 0 AND tournement = ? ) as jugados, 
        (select count(*) from game where tournement = ? AND ((game_type = 1 AND winner = a.id) OR 
        (game_type = 2 and (player_1 = a.id OR player_2 = a.id) and winner = 1) OR
        ( game_type = 2 and (player_3 = a.id OR player_4 = a.id) and winner = 2) )) as ganados 
        from player a ORDER BY ganados DESC 
    '''
    result = con.execute(query, request.form.get('tournementId'))
    tmp_dic = []
    cnt = 0
    for row in result:
        cnt += 1
        tmp_dic.append({
            'id': row[0],
            'name1': row[1],
            'id1': row[2],
            'name2': row[3],
            'id2': row[4],
            'name3': row[5],
            'id3': row[6],
            'name4': row[7],
            'id4': row[8],
            'type': row[9],
            'date': row[10],
            'winner': row[11]
        })
        json_result = json.dumps(tmp_dic, indent=2)
    if cnt > 0:
        return json_result
    else:
        return "[]"


@app.route("/history", methods=['POST'])
def history():
    con = dbConnect()
    cursor = con.cursor()
    query = '''
        create temporary table tmp_table_1 as 
        select id, 
        (select name from player where id = player_1) name1,player_1,
        (select name from player where id = player_2) name2, player_2,
        (select name from player where id = player_3) name3,player_3,
        (select name from player where id = player_4) name4, player_4,
        game_type,date,winner,
        (select number_players from game_type where id = game_type) number_players
        from game where tournement = ? and winner = 0 order by date(date) desc ;
        '''
    con.execute(query, request.form.get('tournementId'))

    query = '''
        insert into tmp_table_1
        select id, 
        (select name from player where id = player_1) name1,player_1,
        (select name from player where id = player_2) name2, player_2,
        (select name from player where id = player_3) name3,player_3,
        (select name from player where id = player_4) name4, player_4,
        game_type,date,winner,
        (select number_players from game_type where id = game_type) number_players
        from game where tournement = ? and winner > 0 order by date(date) desc ;
        '''
    con.execute(query, request.form.get('tournementId'))

    query = ''' select * from tmp_table_1 ; '''
    result = con.execute(query)
    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name1': row[1],
            'id1': row[2],
            'name2': row[3],
            'id2': row[4],
            'name3': row[5],
            'id3': row[6],
            'name4': row[7],
            'id4': row[8],
            'type': row[9],
            'date': row[10],
            'winner': row[11],
            'numberPlayers': row[12]
        })
        json_result = json.dumps(tmp_dic, indent=2)
    con.close()
    return json_result if len(tmp_dic) > 0 else "[]"


@app.route("/profile_picture", methods=["POST"])
def profile_picture():
    con = dbConnect()
    cursor = con.cursor()
    query = "select picture from player where id = ?"
    result = con.execute(query, request.form.get('id'))

    for row in result:
        return send_file(io.BytesIO(row[0]), mimetype='application/octet-stream')


@app.route("/setgamewinner", methods=["POST"])
def set_game_winner():
    con = dbConnect()
    cursor = con.cursor()
    query = "UPDATE game SET winner = ? where id = ?"
    con.execute(query, (request.form.get(
        'winnerId'), request.form.get('gameId')))
    con.commit()

    return make_response(json.dumps({'status': "ok"}), 200)


@app.route("/players", methods=["GET"])
def players():
    con = dbConnect()
    cursor = con.cursor()
    query = "select id,name from player ORDER BY id ASC"
    result = con.execute(query)
    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
        })
        json_result = json.dumps(tmp_dic, indent=2)
    return json_result


def get_team(idPlayer1, idPlayer2):
    query = "SELECT id FROM game_team WHERE player_1 = ? AND player_2 = ?"
    result = con.execute(query, (idPlayer1, idPlayer2))
    if len(result) <= 0:
        # no team registered
        query = "INSERT INTO game_team(player_1,player_2) VALUES(?,?)"
        con.execute(query, (idPlayer1, idPlayer2))
        con.commit()
        return get_team(idPlayer1, idPlayer2)
    else:
        res = 0
        for item in result:
            res = item[0]
        return res


def get_balance(con, idCiv, idPlayer, gameResources, tournement):
    idCiv = int(idCiv)
    if idCiv > 0:
        return idCiv
    import random

    query = "SELECT id FROM civs ORDER BY " + \
        ("rank" if gameResources == 1 else "rank_infinite") + " ASC"
    result = con.execute(query)
    data = []
    for row in result:
        data.append({
            'id': row[0],
        })

    query = "SELECT id,(select count(*) from game where winner = ? AND tournement = ? ) as ganados FROM player ORDER BY ganados"
    result = con.execute(query, (idPlayer, tournement))
    players = []
    for row in result:
        data.append({
            'id': row[0],
        })

    counter = 1
    type = ["S", "A", "B", "C", "D"]
    typeIndex = 0
    bowl = []
    players.reverse()
    indexPlayer = idPlayer
    for item in data:
        bowl.append(item['id'])
        if (indexPlayer == typeIndex):
            bowl.append(item['id'])
            bowl.append(item['id'])
            bowl.append(item['id'])
        counter += 1
        if counter % 6 == 0 and typeIndex < len(type)-1:
            typeIndex += 1

    return random.randint(1, len(bowl))


@app.route("/newgame", methods=["POST"])
def new_game():
    con = dbConnect()
    cursor = con.cursor()
    gameTypeId = int(request.form.get('type'))
    idPlayer1 = request.form.get('id1')
    idPlayer2 = request.form.get('id2')
    idPlayer3 = 0
    idPlayer4 = 0
    resourcesId = request.form.get('resources')

    query = "select max(id) from tournement"
    results = con.execute(query)
    for item in results:
        tournement = item[0]

    idCiv1 = get_balance(con, request.form.get(
        'civ1'), idPlayer1, resourcesId, tournement)
    idCiv2 = get_balance(con, request.form.get(
        'civ2'), idPlayer2, resourcesId, tournement)
    idCiv3 = 0
    idCiv4 = 0
    # check if its team game
    if gameTypeId == 2:
        idPlayer3 = request.form.get('id3')
        idPlayer4 = request.form.get('id4')
        idCiv3 = get_balance(con, request.form.get(
            'civ3'), idPlayer3, resourcesId, tournement)
        idCiv4 = get_balance(con, request.form.get(
            'civ4'), idPlayer4, resourcesId, tournement)
    query = "INSERT INTO game(player_1,player_2, player_3,player_4,civ_1,civ_2,civ_3,civ_4,game_type,tournement,resources,date,winner) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)"
    con.execute(query, (idPlayer1, idPlayer2, idPlayer3, idPlayer4, idCiv1, idCiv2, idCiv3, idCiv4,
                gameTypeId, tournement, resourcesId, request.form.get('date'), request.form.get('idWinner')))
    con.commit()

    return make_response(json.dumps({'status': "ok"}), 200)


@app.route("/deletegame", methods=["POST"])
def delete_game():
    con = dbConnect()
    cursor = con.cursor()
    query = "DELETE FROM game WHERE id = ?"
    con.execute(query, (request.form.get('gameId'),))
    con.commit()

    return make_response(json.dumps({'status': "correct"}), 200)


@app.route("/currenttournement", methods=["GET"])
def currenttournement():
    con = dbConnect()
    cursor = con.cursor()
    query = "SELECT id,name FROM tournement WHERE number = (select max(number) from tournement)"
    result = con.execute(query)

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
        })

    json_result = json.dumps(tmp_dic, indent=2)
    return json_result if len(tmp_dic) > 0 else "[]"


@app.route("/tournements", methods=["GET"])
def all_tournements():
    con = dbConnect()
    cursor = con.cursor()
    query = "SELECT id,name FROM tournement ORDER BY number DESC"
    result = con.execute(query)

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
        })

    json_result = json.dumps(tmp_dic, indent=2)
    return json_result if len(tmp_dic) > 0 else "[]"


@app.route("/civs", methods=["GET"])
def all_civs():
    con = dbConnect()
    cursor = con.cursor()
    query = "SELECT id,name FROM civs ORDER BY name ASC"
    result = con.execute(query)

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
        })

    return jsonify(tmp_dic) if len(tmp_dic) > 0 else "[]"

@app.route("/gametypes", methods=["GET"])
def game_types():
    con = dbConnect()
    cursor = con.cursor()
    query = "SELECT id,name,number_players FROM game_type ORDER BY name ASC"
    result = con.execute(query)

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
            'numberPlayers': row[2],
        })

    return jsonify(tmp_dic) if len(tmp_dic) > 0 else "[]"


@app.route("/resources", methods=["GET"])
def all_resources():
    con = dbConnect()
    cursor = con.cursor()
    query = "SELECT id,resources FROM game_resources ORDER BY resources ASC"
    result = con.execute(query)

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'id': row[0],
            'name': row[1],
        })

    return jsonify(tmp_dic) if len(tmp_dic) > 0 else "[]"


@app.route("/game/<int:id_game>", methods=["GET"])
def game(id_game):
    con = dbConnect()
    cursor = con.cursor()
    query = '''
        select date,
        case when winner > 0 and game_type = 1 then (select name from player where id = winner) else winner end winner,
        (select name from game_type where id = game_type) game_type ,
        (select name from player where id = player_1) as name1,
        (select name from civs where id = civ_1) as civ_1,
        (select name from player where id = player_2) as name2,
        (select name from civs where id = civ_2) as civ_2,
        (select name from player where id = player_3) as name3,
        (select name from civs where id = civ_3) as civ_3,
        (select name from player where id = player_4) as name4,
        (select name from civs where id = civ_4) as civ_4,
        (select resources from game_resources where id = game.resources) as resources,
        player_1,player_2,player_3,player_4,
        (select number_players from game_type where id = game_type) number_players
        from game where id = ?
    '''
    result = con.execute(query, (id_game,))

    tmp_dic = []
    for row in result:
        tmp_dic.append({
            'date': row[0],
            'winner': 'PENDIENTE' if row[1] == 0 else row[1] ,
            'gameType': row[2],
            'name1': row[3],
            'civ1': row[4],
            'name2': row[5],
            'civ2': row[6],
            'name3': row[7],
            'civ3': row[8],
            'name4': row[9],
            'civ4': row[10],
            'resources': row[11],
            'id1': row[12],
            'id2': row[13],
            'id3': row[14],
            'id4': row[15],
            'numberPlayers': row[16],
        })

    return jsonify(tmp_dic) if len(tmp_dic) > 0 else "[]"
